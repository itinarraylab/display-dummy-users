# Display Dummy Users

Wordpress plugin to display users from dummy API on custom page

## System Requirements

| Name      | Version |
|-----------|---------|
| PHP       |  7.3+   |
| Wordpress | 4.9.6+  |

To install project from source instead of simple zip archive, you will need [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).  
To manage dependencies, you need [Composer](https://getcomposer.org/download/)

## Plugin Installation

### Downloading from the source
Clone this project to `wp-content/plugins` folder

```bash
cd wp-content/plugins
git clone https://gitlab.com/itinarraylab/display-dummy-users.git .
```


### Dependency management
Run composer update command in Wordpress root folder to install composer dependencies.

```bash
 php composer.phar --working-dir=wp-content/plugins/display-dummy-users update
```
### Setting Composer autoloader
Add `<path>` to plugin composer `autoload.php` in `wp-config.php` somewhere after `ABSPATH` constant definition. 
Your `wp-config.php` should look similar to the following:

```php
/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Display Dummy Users autoloader path. */
if (file_exists(ABSPATH . 'wp-content/plugins/display-dummy-users/vendor/autoload.php')) {
    require_once( ABSPATH . 'wp-content/plugins/display-dummy-users/vendor/autoload.php');
}
```
### Enabling Cache (optional)

File cache could be used for all API requests.

To enable the cache, you need to create a folder or use an existing one where you want all cache files to be saved. 
>Make sure the folder is writable for the web server. For more information use [Changing File Permissions](https://wordpress.org/support/article/changing-file-permissions/) Wordpress article.

*Notes: Files in the cache folder will be deleted on plugin deactivation.*

Once a writable folder exists, define `DDU_CACHE_DIR` constant in `wp-config.php` e.g  `wp-content/cache` is created to store cached files and it is writable by web server, 
your `wp-config.php` should look like the following:

```php
/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Enables Display Dummy Users cache. */
if ( ! defined( 'DDU_CACHE_DIR' ) ) {
	define( 'DDU_CACHE_DIR', ABSPATH . 'wp-content/cache/');
}

/** Display Dummy Users autoloader path. */
if (file_exists(ABSPATH . 'wp-content/plugins/display-dummy-users/vendor/autoload.php')) {
    require_once( ABSPATH . 'wp-content/plugins/display-dummy-users/vendor/autoload.php');
}
```

 In this version there are some default settings for cache which cannot be overwritten:
```php
 
    /**
     * Cache time (s)
     *
     * @var int
     */
    private $cacheTime = 3600;

    /**
     * Cache file extension
     *
     * @var string
     */
    private $cacheExtension = '.json';

    /**
     * Cache file extension
     *
     * @var string
     */
    private $cachePrefix = 'ddu_cache_';

```

## Plugin Activation

Find `Display Dummy Users` in the `Dashboard` navigating to `Plugins > Installed Plugins` and click `Activate`

## Plugin Usage
Upon uccessful activation, navigate in the browser to your Wordpress url with a param `display_dummy_users` and set the value `1` e.g `<wordpress url>/?display_dummy_users=1`. 
You should be able to see the user table similar to the following:

| ID            | Name          | Username |
| ------------- |:-------------:| -----:   |
| 1             | Leanne Graham | Bret     |
| 2             | Ervin Howell  | Antonette|
| ...           | ...           | ...      |  

To find a user easier, you can sort or filter users in the table. 
If you click on any user's `<ID>`, `<Name>` or `<Username>`, you should see details for that user on top of the page.

*Notes: If something happens during the plugin execution, you should see a detailed Error message*

## Plugin Deactivation

Find `Display Dummy Users` in the `Dashboard` navigating to `Plugins > Installed Plugins` and click `Deactivate`
*Notes: All files in the cache folder, if any, with the prefix and extension related to this plugin will be deleted.*

## Code Standards

### Installation

The code styles are enforced via the popular [`php_codesniffer`](https://packagist.org/packages/squizlabs/php_codesniffer)
and can be installed via Composer by the name **`inpsyde/php-coding-standards`**.

It has been added to composer.json `require-dev`:

```json
{
	"require-dev": {
		"inpsyde/php-coding-standards": "^0.13"
	}
}
```

### Usage

When the package is installed via Composer, and dependencies are updated, everything is
ready and the coding standards can be checked via running the following command inside the plugin folder:
```
$ vendor/bin/phpcs
```
or 

```
$ composer run-script check-standards
```
Configuration file with all advanced settings is located in the root folder of the plugin `phpcs.xml`.

Files to check were added:

```xml
  <file>./src</file>
  <file>./ddu-config.php</file>
  <file>./display-dummy-users.php</file>
```
 
Some files were excluded from check, e.g minified `.js`, `.css` files in `src/public/assets` folder:

```xml
  <exclude-pattern>*/src/public/assets/*min\.(css|js)$</exclude-pattern>
```

There are no requirements for this plugin to check content of ./tests folder.

## Unit Testing

Display Dummy Users uses PHPUnit and Brain Monkey as unit test utilities. 

The latest PHP_Unit documentations you can find [here](https://phpunit.readthedocs.io/en/9.1/).  
The latest Brain Monkey documentations you can find [here](https://brain-wp.github.io/BrainMonkey/)

### Installation

PHP_Unit is available on Packagist and can be installed via Composer by the name **`phpunit/phpunit`**. 
Brain Monkey is available on Packagist and can be installed via Composer by the name **`brain/monkey`**.

It has been added to composer.json `require-dev`:

```json
{
    "require-dev": {
        "brain/monkey": "^2.0@dev",
        "phpunit/phpunit": "^9.0"
    }
}
```

### Usage

When the packages are installed via Composer, and dependencies are updated, everything is
ready for unit tests, just run the following command inside plugin folder:

```
$ vendor/bin/phpunit
```
or 

```
$ composer run-script test
```
Configuration file with all advanced settings is located in the root folder of the plugin `phpunit.xml.dist`


## Implementation Details

### Project Structure

Project structure was designed to reduce coupling, increase cohesion, be clean, logical and follow PSR-4 autoloader recommendations. 
In the top-level "vendor namespace" DisplayDummyUsers there are Load and Deactivate classes (Activate could be used here too if needed).  
All services are located in the Services, Interfaces in Interfaces etc. 
Folder `src\public` contains all assets and files for the website and not for administration dashboard.

### Interfaces

Some classes extended by other classes implement Interfaces. E.g Api, Cache, Normalize.  

### Cache

There are many ways to implement cache. Client storage in browser, database, Memcache, Redis could be use. They all have their advantages and disadvantages. 
File cache was picked here as it is very easy to set up, without creating connection to the database and do not require some extra settings for servers if some packages are not installed.

### Frontend

To keep everything consistent AJAX was used to display the users table and user details.
Due to flexible requirements all scripts and styles are independent from Wordpress. Function `wp_head()` and `wp_footer()` as well as  `wp_enqueue_script()` and `wp_enqueue_style()` could be used id needed.
To improve the table look and feel, 3rd party libraries have been used: [jQuery 3.5](https://jquery.com/download/), [bootstrap 4.3](https://getbootstrap.com/docs/4.3/getting-started/introduction/), [tablesorter](https://mottie.github.io/tablesorter/docs/example-option-theme-bootstrap-v4.html). 
For simplicity, frontend assets were shipped "compiled", no `npm` or `webpack` were used.
All frontend logic is in `assets/js/scripts.js`.  


### Other

* Wordpress functionality used in Plugin: 

  * *Objects* : `\WP_Query`
  * *Functions* : `wp_remote_get`, `is_wp_error`, `wp_send_json_success`, `wp_send_json_error`
  * *Hooks* :`add_action`, `add_filter`

* In the Ajax class `usleep` was used to prevent multiple repetitive clicks to get user details.
* Unit tests are not covering 100% of the code as there is no such requirement.
* ./tests excluded from code standards validation.
 

