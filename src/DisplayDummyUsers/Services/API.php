<?php declare(strict_types = 1);
/**
 * Class API
 * PHP version 7.3
 */
namespace DisplayDummyUsers\Services;

use DisplayDummyUsers\Interfaces\APIInterface;

/**
 * Class API
 *
 * @category Wordpress_Plugin
 * @package  DisplayDummyUsers
 * @author   Kostiantyn Aleksieiev <const.ca@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://www.linkedin.com/in/kostiantyn-aleksieiev-3769898b/
 */
class API implements APIInterface
{

    const BASE_URL = 'https://jsonplaceholder.typicode.com/';
    const TIMEOUT = 30;
    const PATH_SEP = '/';

    /**
     * @var bool
     */
    private $cache;
    /**
     * @var array
     */
    private $args = [];

    /**
     * API constructor.
     */
    public function __construct()
    {
        $this->cache = new Cache();
        $this->args =  [
            'headers' => [
                'Content-Type' => 'application/json',
                'timeout' => self::TIMEOUT,
            ],
        ];
    }

    /**
     * Creates request to get one users
     *
     * @param string $resource           'resource name'
     * @param string $resourceIdentifier 'resource id'
     *
     * @return array
     */
    public function fetch(string $resource, string $resourceIdentifier = null): array
    {
        $url = $this->buildUrl($resource, $resourceIdentifier);
        if (!$this->cache->isCacheEnabled()) {
            return $this->fetchAPI($url);
        }
        return $this->fetchCached($url);
    }

    /**
     * Builds API URL
     *
     * @param string      $resource           'resource name'
     * @param string|null $resourceIdentifier 'resource id'
     *
     * @return string
     */
    public function buildUrl(
        string $resource,
        string $resourceIdentifier = null
    ): string {

        $id = '';
        if (!is_null($resourceIdentifier)) {
            $id = self::PATH_SEP.$resourceIdentifier;
        }

        return self::BASE_URL.$resource.$id;
    }

    /**
     * Setting request arguments
     *
     * @param array $args 'Optional. Request arguments. Default empty array.'
     *
     * @return void
     */
    public function changeArgs(array $args)
    {
        $this->args = wp_parse_args($args, $this->args);
    }

    /**
     * Fetch and prepare response from API
     *
     * @param string $url 'api url'
     *
     * @return array
     */
    public function fetchAPI(string $url) :array
    {
        $response = $this->get($url);
        if (isset($response['error'])) {
            return $response;
        }
        return json_decode($response['body'], true);
    }

    /**
     * Fetch and prepare response from cache or API
     *
     * @param string $url 'api url'
     *
     * @return array
     */
    public function fetchCached(string $url) :array
    {
        $label = md5($url);
        $cachedData = $this->cache->read($label);

        if ($cachedData !== '') {
            return json_decode($cachedData, true);
        }
        $response = $this->get($url);
        if (isset($response['error'])) {
            return $response;
        }
        $this->cache->write($label, $response['body']);
        return json_decode($response['body'], true);
    }

    /**
     * Creates request to get one users
     *
     * @param string $url 'api url'
     *
     * @return array|\WP_Error
     */
    public function get(string $url): array
    {
        $response = wp_remote_get($url, $this->args);
        if (is_wp_error($response)) {
            return ['error'=>$response->get_error_message()];
        }
        return $response;
    }
}
