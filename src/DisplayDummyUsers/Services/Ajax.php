<?php declare(strict_types = 1);

/**
 * Class Ajax
 * PHP version 7.3
 */
namespace DisplayDummyUsers\Services;

use DisplayDummyUsers\Entities\Users;
use DisplayDummyUsers\Normalizers\NormalizeUsers;

/**
 * Class Ajax
 *
 * @category Wordpress_Plugin
 * @package  DisplayDummyUsers
 * @author   Kostiantyn Aleksieiev <const.ca@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://www.linkedin.com/in/kostiantyn-aleksieiev-3769898b/
 */
class Ajax
{
    /**
     * Action hook used by the AJAX class.
     *
     * @var string
     */
    const ACTION = 'display_dummy_users';

    /**
     * Action argument used by the nonce validating the AJAX request.
     *
     * @var string
     */
    const NONCE = 'dummy-users-ajax';

    /**
     * Admin url argument used as ajax_url in AJAX request.
     *
     * @var string
     */

    const ADMIN_URL = 'admin-ajax.php';

    /**
     * Delay 0.25s for all AJAX requests.
     *
     * @var string
     */
    const US_MS = 250000;

    /**
     * Register the AJAX handler class with all the appropriate WordPress hooks.
     *
     * @return void
     */
    public function register()
    {
        add_action('wp_ajax_' . self::ACTION, [$this, 'handle']);
        add_action('wp_ajax_nopriv_' . self::ACTION, [$this, 'handle']);
    }

    /**
     * Handles the AJAX request for my plugin.
     */
    public function handle()
    {
        // Make sure we are getting a valid AJAX request
        check_ajax_referer(self::NONCE);
        usleep(self::US_MS);
        $status = filter_input(INPUT_GET, 'status');

        $users = new Users();
        switch ($status) {
            case 'many':
                $response = $users->many();
                $extraMessage = sprintf(
                    '<strong> %s</strong>',
                    __('Please refresh the page', 'ddu')
                );
                $nResponse = NormalizeUsers::many($response);
                $this->sendResponse($nResponse, $extraMessage);
                break;
            case 'one':
                $userId = filter_input(INPUT_GET, 'user_id', FILTER_SANITIZE_NUMBER_INT);
                $response = $users->one((int)$userId);
                $nResponse = NormalizeUsers::one($response);
                $this->sendResponse($nResponse);
                break;
            default:
                wp_send_json_error(
                    sprintf(
                        '<strong>%s</strong>Your request was not recognized. Try a different one!',
                        __('Oh no...', 'ddu')
                    )
                );
        }
    }

    /**
     * Sends response to UI
     *
     * @param array  $response     'response from API'
     * @param string $extraMessage 'to customize an error message'
     *
     * @return void
     */
    private function sendResponse(array $response, string $extraMessage = '')
    {
        if (isset($response['error'])) {
            wp_send_json_error($response['error'].$extraMessage);
            return ;
        }
        if ($response) {
             wp_send_json_success($response);
            return ;
        }

            wp_send_json_error(
                sprintf(
                    '<strong>%s</strong> Nothing was found. Try to modify request!',
                    __('Oh, no...', 'ddu')
                )
            );
    }

    /**
     * Get the AJAX data that WordPress needs to output.
     *
     * @return array
     */
    public function ajaxData(): array
    {
        return [
            'ajaxAction' => self::ACTION,
            'ajaxNonce' => wp_create_nonce(self::NONCE),
            'ajaxUrl' => admin_url(self::ADMIN_URL),
        ];
    }
}
