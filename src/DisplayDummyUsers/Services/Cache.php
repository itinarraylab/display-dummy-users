<?php declare(strict_types = 1);
/**
 * Class Cache
 * PHP version 7.3
 */
namespace DisplayDummyUsers\Services;

use DisplayDummyUsers\Interfaces\CacheInterface;

/**
 * Class API
 *
 * @category Wordpress_Plugin
 * @package  DisplayDummyUsers
 * @author   Kostiantyn Aleksieiev <const.ca@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://www.linkedin.com/in/kostiantyn-aleksieiev-3769898b/
 */
class Cache implements CacheInterface
{

    /**
     * Cache time (s)
     *
     * @var int
     */
    private $cacheTime = 3600;

    /**
     * Cache file extension
     *
     * @var string
     */
    private $cacheExtension = '.json';

    /**
     * Cache file extension
     *
     * @var string
     */
    private $cachePrefix = 'ddu_cache_';

    /**
     * To write data in cache
     *
     * @param string $label 'hash of get params and uid'
     * @param string $data  'json format'
     *
     * @return void
     */
    public function write(string $label, string $data)
    {
        @file_put_contents($this->cacheDir() . DIRECTORY_SEPARATOR .
                          $this->safeFileName($this->cachePrefix.$label)
                          .$this->cacheExtension, $data);
    }

    /**
     * Returns json from cache or empty
     * @param string $label
     *
     * @return array|bool|false|string
     */
    public function read(string $label): string
    {
        $return = '';
        if (!$this->isCached($label)) {
            return $return;
        }
            $filename = $this->cacheDir() .DIRECTORY_SEPARATOR
                        .$this->safeFileName($this->cachePrefix.$label)
                        . $this->cacheExtension;
        try {
            $fileContent = file_get_contents($filename);
            if (!$fileContent) {
                return $return;
            }

            return $fileContent;
        } catch (\Throwable $exception) {
            return $return;
        }
    }

    /**
     * Flushes cache
     *
     * @return bool
     */
    public function flush() :bool
    {
        if (is_dir($this->cacheDir())) {
            $filesPattern = $this->cacheDir().DIRECTORY_SEPARATOR
                            .$this->cachePrefix."*".$this->cacheExtension;
            try {
                array_map('unlink', glob($filesPattern));
                return true;
            } catch (\Throwable $exception) {
                return false;
            }
        }
        return false;
    }

    /**
     * Check if file exists and not expired
     *
     * @param string $label 'file name'
     *
     * @return bool
     */
    public function isCached(string $label) :bool
    {
        $filename = $this->cacheDir() .DIRECTORY_SEPARATOR
                    . $this->safeFileName($this->cachePrefix.$label)
                    . $this->cacheExtension;

        if (file_exists($filename)
            && (filemtime($filename) + $this->cacheTime >= time())
        ) {
            return true;
        }

        return false;
    }

    /**
     * Helper for sanitizing filename
     *
     * @param string $filename 'filename hash'
     *
     * @return string
     */
    private function safeFileName(string  $filename): string
    {
        return preg_replace('/[^0-9a-z\.\_\-]/i', '', strtolower($filename));
    }

    /**
     * Checks if folder for cache created and writable
     *
     * @return bool
     */
    public function isCacheEnabled(): bool
    {
        if (!defined('DDU_CACHE_DIR')) {
            return false;
        }

        if (!is_dir(DDU_CACHE_DIR)) {
            return false;
        }

        if (!is_writable(DDU_CACHE_DIR)) {
            return false;
        }

        return true;
    }

    /**
     * Returns file path or empty string
     *
     * @return string
     */
    public function cacheDir(): string
    {
        if ($this->isCacheEnabled()) {
            return preg_replace('/\/$/', '', DDU_CACHE_DIR);
        }

        return '';
    }
}
