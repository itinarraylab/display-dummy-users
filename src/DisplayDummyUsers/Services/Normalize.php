<?php  declare(strict_types = 1);
/**
 * Class Normalize
 * PHP version 7.3
 */

namespace DisplayDummyUsers\Services;

use DisplayDummyUsers\Interfaces\NormalizeInterface;

/**
 * Class Normalize to standardize responses for UI
 *
 * @category Wordpress_Plugin
 * @package  DisplayDummyUsers
 * @author   Kostiantyn Aleksieiev <const.ca@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://www.linkedin.com/in/kostiantyn-aleksieiev-3769898b/
 */

class Normalize implements NormalizeInterface
{

    /**
     * Normalizing response to use in frontend
     *
     * @param array $data | response from API for many
     *
     * @return array
     */
    public static function many(array $data): array
    {
        return $data;
    }

    /**
     * Normalizing response to use in frontend
     *
     * @param array $data | response from API for one
     *
     * @return array
     */
    public static function one(array $data): array
    {
        return $data;
    }

    /**
     * Checks if response is not an error message
     *
     * @param $data 'data received'
     *
     * @return bool
     */
    public static function hasError(array $data): bool
    {
        return isset($data['error']) || empty($data);
    }
}
