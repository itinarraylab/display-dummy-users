<?php declare(strict_types = 1);

namespace DisplayDummyUsers\Services;

/**
 * Class Loader
 *
 * @category Wordpress_Plugin
 * @package  DisplayDummyUsers
 * @author   Kostiantyn Aleksieiev <const.ca@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://www.linkedin.com/in/kostiantyn-aleksieiev-3769898b/
 */
class Loader
{
    const PUBLIC_PATH = 'src/public';

    /**
     * Builds and returns path to public folder
     *
     * @return string
     */
    protected function getPublicPath(): string
    {
        $pluginPath = $this->getPluginPath();
        return $pluginPath.self::PUBLIC_PATH.DIRECTORY_SEPARATOR;
    }

    /**
     * Builds and returns path to public folder
     *
     * @return string
     */
    private function getPluginPath(): string
    {
        return DISPLAY_DUMMY_USERS_PATH.DIRECTORY_SEPARATOR;
    }

    /**
     * Loading content of the file based on file name provided in view param
     *
     * @param string $view   'filename in public'
     * @param array  $params 'to be passed to view'
     *
     * @return bool
     */
    public function render(String $view, array $params = []): bool
    {
        if (file_exists(
            $this->getPublicPath() .$view.'.php'
        )
        ) {
            return (bool) require_once $this->getPublicPath() .$view.'.php';
        }
        return false;
    }
}
