<?php declare(strict_types = 1);
namespace DisplayDummyUsers;

/**
 * Class Activate
 * PHP version 7.3
 */

use DisplayDummyUsers\Services\Loader;
use DisplayDummyUsers\Services\Ajax;

/**
 * Class Activate
 *
 * @category Wordpress_Plugin
 * @package  DisplayDummyUsers
 * @author   Kostiantyn Aleksieiev <const.ca@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://www.linkedin.com/in/kostiantyn-aleksieiev-3769898b/
 */
class Load extends Loader
{
    const MY_URL = 'display_dummy_users';

    private $params;

    /**
     * Initialization of the plugin
     *
     * @return void;
     */

    public static function init()
    {
        $self = new self();
        $ajax = new Ajax();
        $ajax->register();
        $self->params = $ajax->ajaxData();
        add_filter('query_vars', [ $self, 'addQueryVars' ]);
        add_action('template_redirect', [ $self, 'userTemplate' ]);
    }

    /**
     * Avoiding coupling to global variable
     *
     * @return \WP_Query
     */
    public function wpQuery(): \WP_Query
    {
        global $wp_query;

        return $wp_query;
    }

    /**
     * Add the 'my_url' query variable so WordPress won't remove it.
     *
     * @param array $vars 'query variable'
     *
     * @return array
     */
    public function addQueryVars(array $vars): array
    {
        $vars[] = self::MY_URL;

        return $vars;
    }

    /**
     * Check if 'my_url' query variable found and load output or template otherwise
     *
     * @param string $template 'wordpress template'
     *
     * @return string|void
     */
    public function userTemplate(string $template): ?string
    {
        $wpQuery = $this->wpQuery();

        if (! isset($wpQuery->query[self::MY_URL])) {
            return $template;
        }
        $userTemplate = $wpQuery->query[self::MY_URL];
        if (true === $this->output($userTemplate)) {
            die(null);
        }
        return $template;
    }

    /**
     * Loading output
     *
     * @param string $value 'get attribute'
     *
     * @return bool
     */
    public function output(string $value) : bool
    {
        switch ($value) {
            case '1':
                return $this->render('dummy-users', $this->params);
        }

        return false;
    }
}
