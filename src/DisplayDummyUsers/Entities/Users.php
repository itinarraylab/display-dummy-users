<?php declare(strict_types = 1);
/**
 * Class API
 * PHP version 7.3
 */
namespace DisplayDummyUsers\Entities;

use DisplayDummyUsers\Services\API;

/**
 * Class API
 *
 * @category Wordpress_Plugin
 * @package  DisplayDummyUsers
 * @author   Kostiantyn Aleksieiev <const.ca@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://www.linkedin.com/in/kostiantyn-aleksieiev-3769898b/
 */
class Users extends API
{

    /**
     * Creates request to get many users
     *
     * @return array
     */
    public function many() : array
    {
        return $this->fetch('users');
    }

    /**
     * Creates request to get one user
     *
     * @param int $id 'user id to get'
     *
     * @return array
     */
    public function one(int $id): array
    {
        return $this->fetch('users', (string)$id);
    }
}
