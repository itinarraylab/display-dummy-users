<?php declare(strict_types = 1);
/**
 * Class Deactivate
 * PHP version 7.3
 */
namespace DisplayDummyUsers;

use DisplayDummyUsers\Services\Cache;

/**
 * Class Deactivate
 *
 * @category Wordpress_Plugin
 * @package  DisplayDummyUsers
 * @author   Kostiantyn Aleksieiev <const.ca@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://www.linkedin.com/in/kostiantyn-aleksieiev-3769898b/
 */
class Deactivate
{

    /**
     * Initialization of the plugin
     *
     * @return void;
     */

    public static function init()
    {
        $self = new self();
        $self->clearCache();
    }

    /**
     * On deactivating plugin remove cached files
     * @return bool
     */
    private function clearCache(): bool
    {
        $cache = new Cache();
        return $cache->flush();
    }
}
