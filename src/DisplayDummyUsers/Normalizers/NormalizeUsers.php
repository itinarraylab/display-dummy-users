<?php  declare(strict_types = 1);
/**
 * Class Normalize
 * PHP version 7.3
 */

namespace DisplayDummyUsers\Normalizers;

use DisplayDummyUsers\Interfaces\NormalizeInterface;
use DisplayDummyUsers\Services\Normalize;

/**
 * Class Normalize to standardize responses for UI
 *
 * @category Wordpress_Plugin
 * @package  DisplayDummyUsers
 * @author   Kostiantyn Aleksieiev <const.ca@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://www.linkedin.com/in/kostiantyn-aleksieiev-3769898b/
 */

class NormalizeUsers extends Normalize implements NormalizeInterface
{

    /**
     * Normalizing response to use in frontend
     *
     * @param array $data | response from API for many
     *
     * @return array
     */
    public static function many(array $data): array
    {
        if (self::hasError($data)) {
            return $data;
        }
        $users = [];
        if (is_countable($data) && count($data) !== count($data, COUNT_RECURSIVE)) {
            $i = 0;
            foreach ($data as $user) {
                $users[$i]['id'] = (!empty($user['id'])) ? $user['id'] : '';
                $users[$i]['name'] = (!empty($user['name'])) ? $user['name'] : '';
                $users[$i]['username'] = (!empty($user['username']))
                    ? $user['username'] : '';
                $i++;
            }
        }

        return $users;
    }

    /**
     * Normalizing response to use in frontend
     *
     * @param array $data | response from API for one
     *
     * @return array
     */
    public static function one(array $data): array
    {
        if (self::hasError($data)) {
            return $data;
        }
        $user = [];
        
        $user['id'] =  (!empty($data['id'])) ? $data['id'] : '';
        $user['name'] =  (!empty($data['name'])) ? $data['name'] : '';
        $user['username'] =  (!empty($data['username'])) ? $data['username'] : '';
        $user['email'] =  (!empty($data['email'])) ? $data['email'] : '';
        $user['phone'] =  (!empty($data['phone'])) ? $data['phone'] : '';
        $user['phoneFormatted'] =  (!empty($data['phone']))
            ? self::formatPhone($data['phone']) : '';
        $user['addressFormatted'] =  (!empty($data['address']))
            ? self::formatAddress((array)$data['address']) : '';
        $user['coordinates'] =  (!empty($data['address']))
            ? self::formatCoordinates($data['address']) : '';
        $user['website'] =  (!empty($data['website'])) ? $data['website'] : '';
        $user['companyName'] =  (!empty($data['company']))
            ? self::companyAttribute($data['company'], 'name') : '';
        $user['companyCatchPhrase'] =  (!empty($data['company']))
            ? self::companyAttribute($data['company'], 'catchPhrase') : '';
        $user['companyBs'] =  (!empty($data['company']))
            ? self::companyAttribute($data['company'], 'bs') : '';
        $user['websiteFormatted'] =  (!empty($data['website']))
            ? self::formatWebsite($data['website']) : '';

        return $user;
    }

    /**
     * Phone for attribute href='tel:formattedPhone'
     *
     * @param string $phone 'raw phone'
     *
     * @return string
     */
    private static function formatPhone(string $phone) : string
    {
        $phone = preg_replace('@\s?x\s?@', 'p', $phone);
        $phone = preg_replace('@\s?\(\s?@', '', $phone);
        $phone = preg_replace('@\s?\)\s?@', '-', $phone);
        return preg_replace('@\s@', '-', $phone);
    }

    /**
     * Website for attribute href='formattedUrl'
     * URL with a relative (unspecified) protocol
     *
     * @param string $website 'raw website'
     *
     * @return string
     */
    private static function formatWebsite(string $website): string
    {
        $website = preg_replace('#^https?://#', '', $website);
        return '//'.$website;
    }

    /**
     * One line address
     *
     * @param array $address 'address as array'
     *
     * @return string
     */
    private static function formatAddress(array $address): string
    {
        $street = (!empty($address['street'])) ? $address['street'] : '';
        $suite = (!empty($address['suite'])) ? $address['suite'] : '';
        $city = (!empty($address['city'])) ? $address['city'] : '';
        $zip = (!empty($address['zipcode'])) ? $address['zipcode'] : '';
        $formattedAddress = $street.' '.$suite.' '.$city.' '.$zip;
        $formattedAddress = preg_replace('@(\s)+@', ' ', trim($formattedAddress));
        return  preg_replace('@^\s$@', '', $formattedAddress);
    }

    /**
     * One line coordinates
     *
     * @param array $address 'address as array'
     *
     * @return string
     */
    private static function formatCoordinates(array $address): string
    {
        if (isset($address['geo'])) {
            $geo = $address['geo'];
            $lat = (!empty($geo['lat'])) ? $geo['lat'] : '';
            $lng = (!empty($geo['lng'])) ? $geo['lng'] : '';
            if ($lat && $lng) {
                return $lat.', '.$lng;
            }
        }

        return '';
    }

    /**
     * Returns company attribute or empty string
     *
     * @param array  $company 'company data'
     * @param string $attr    'attribute name'
     *
     * @return string
     */
    private static function companyAttribute(array $company, string $attr): string
    {
        return (!empty($company[$attr])) ? $company[$attr]: '';
    }
}
