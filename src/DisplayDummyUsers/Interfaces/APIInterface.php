<?php declare(strict_types = 1);
/**
 * Interface APIInterface
 * PHP version 7.3
 */
namespace DisplayDummyUsers\Interfaces;

/**
 * Interface InterfaceAPI
 *
 * @category Wordpress_Plugin
 * @package  DisplayDummyUsers
 * @author   Kostiantyn Aleksieiev <const.ca@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://www.linkedin.com/in/kostiantyn-aleksieiev-3769898b/
 */
interface APIInterface
{

    /**
     * Creates request to get one users
     *
     * @param string $resource           'resource name'
     * @param string $resourceIdentifier 'resource id'
     *
     * @return array
     */
    public function fetch(string $resource, string $resourceIdentifier = null);

    /**
     * Builds API URL
     *
     * @param string      $resource           'resource name'
     * @param string|null $resourceIdentifier 'resource id'
     *
     * @return string
     */
    public function buildUrl(string $resource, string $resourceIdentifier = null);

    /**
     * Setting request arguments
     *
     * @param array $args 'Optional. Request arguments. Default empty array.'
     *
     * @return array
     */
    public function changeArgs(array $args);

    /**
     * Creates request to get one users
     *
     * @param string $url 'api url'
     *
     * @return array|\WP_Error
     */
    public function get(string $url);
}
