<?php declare(strict_types = 1);
/**
 * Interface InterfaceAPI
 * PHP version 7.3
 */
namespace DisplayDummyUsers\Interfaces;

/**
 * Interface CacheInterface
 *
 * @category Wordpress_Plugin
 * @package  DisplayDummyUsers
 * @author   Kostiantyn Aleksieiev <const.ca@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://www.linkedin.com/in/kostiantyn-aleksieiev-3769898b/
 */
interface CacheInterface
{
    /**
     * Creates request to get all users
     *
     * @return array
     */
    public function read(string $label);

    /**
     * To write data in cache
     *
     * @param string $label 'hash of get params and uid'
     * @param string $data  'json format'
     *
     * @return void
     */
    public function write(string $label, string $data);

    /**
     * Check if file exists and not expired
     *
     * @param string $label 'file name'
     *
     * @return bool
     */
    public function isCached(string $label);
}
