<?php  declare(strict_types = 1);
/**
 * Interface InterfaceNormalize
 * PHP version 7.3
 */
namespace DisplayDummyUsers\Interfaces;

/**
 * Interface NormalizeInterface
 *
 * @category Wordpress_Plugin
 * @package  DisplayDummyUsers
 * @author   Kostiantyn Aleksieiev <const.ca@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://www.linkedin.com/in/kostiantyn-aleksieiev-3769898b/
 */
interface NormalizeInterface
{

    /**
     * Normalizing response to use in frontend
     *
     * @param array $data | response from API for many
     *
     * @return array
     */
    public static function many(array $data);

    /**
     * Normalizing response to use in frontend
     *
     * @param array $data | response from API for one
     *
     * @return array
     */
    public static function one(array $data);

    /**
     * Checks if response is not an error message
     *
     * @param $data 'data received'
     *
     * @return bool
     */
    public static function hasError(array $data);
}
