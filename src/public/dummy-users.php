<?php declare(strict_types = 1);
$templateUrl = plugin_dir_url(__FILE__);
$params = ( ! empty($params) ) ? $params : null;
$ajaxUrl = ($params['ajaxUrl']) ? $params['ajaxUrl']: '';
$ajaxNonce = ($params['ajaxNonce']) ? $params['ajaxNonce']: '';
$ajaxAction = ($params['ajaxAction']) ? $params['ajaxAction']: '';

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dummy Dummy Users Users</title>

    <link rel="stylesheet"
          href="<?php
            echo esc_url($templateUrl);
            ?>assets/lib/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="<?php
            echo esc_url($templateUrl);
            ?>assets/lib/tablesorter/2.31.3/css/themes.bootstrap.min.css">
    <link rel="stylesheet"
          href="<?php
            echo esc_url($templateUrl);
            ?>assets/lib/tablesorter/2.31.3/css/jq.min.css">
    <link rel="stylesheet"
          href="<?php
            echo esc_url($templateUrl);
            ?>assets/lib/tablesorter/2.31.3/css/jquery.tablesorter.pager.min.css">
    <link rel="stylesheet"
          href="<?php echo esc_url($templateUrl); ?>assets/css/style.css">

</head>

<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="#" class="user-details">Dummy Users</a>


</nav>

<main role="main" class="container">
    <div class="starter-template">
        <h2>Dummy Users Plugin</h2>
    </div>
    <div id="dummy-user-details-container">
        <div id="loadingDiv" class="center hidden">
            <img height="70"
                 src="<?php echo esc_url($templateUrl); ?>assets/images/loading.gif"
                 alt="Loading...">
        </div>
    <div id="dummy-user-details" >

    </div>
    </div>
    <table id="user-table" class="table table-bordered table-striped hidden">
        <thead class="thead-dark"> <!-- add class="thead-light" for a light header -->
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Username</th>

        </thead>
        <tfoot>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Username</th>

        </tr>
        <tr>
            <th colspan="3" class="ts-pager">
                <div class="form-inline">
                    <div class="btn-group btn-group-sm mx-1" role="group">
                        <button type="button" class="btn btn-secondary first" title="first">⇤</button>
                        <button type="button" class="btn btn-secondary prev" title="previous">←</button>
                    </div>
                    <span class="pagedisplay"></span>
                    <div class="btn-group btn-group-sm mx-1" role="group">
                        <button type="button" class="btn btn-secondary next" title="next">→</button>
                        <button type="button" class="btn btn-secondary last" title="last">⇥</button>
                    </div>
                    <select class="form-control-sm custom-select px-1 pagesize" title="Select page size">
                        <option selected="selected" value="5">5</option>
                        <option value="10">10</option>
                        <option value="all">All Rows</option>
                    </select>
                    <select
                        class="form-control-sm custom-select px-4 mx-1 pagenum"
                        title="Select page number"></select>
                    <button
                        type="button"
                        class="reset btn btn-sm btn-secondary">Reset Filters</button>
                </div>

            </th>
        </tr>
        </tfoot>
        <tbody id="table-content">

        </tbody>
    </table>
<input type="hidden" id="ajax-settings"
       data-ajax-url="<?php echo esc_attr($ajaxUrl); ?>"
       data-ajax-action="<?php echo esc_attr($ajaxAction); ?>"
       data-ajax-nonce="<?php echo esc_attr($ajaxNonce); ?>"
>
</main><!-- /.container -->
<script src="<?php
echo esc_url($templateUrl);
?>assets/lib/jquery/3.5.0/jquery.min.js"></script>
<script src="<?php
echo esc_url($templateUrl);
?>assets/lib/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="<?php
echo esc_url($templateUrl);
?>assets/lib/tablesorter/2.31.3/js/jquery.tablesorter.min.js"></script>
<script src="<?php
echo esc_url($templateUrl);
?>assets/lib/tablesorter/2.31.3/js/jquery.tablesorter.widgets.min.js"></script>
<script src="<?php
echo esc_url($templateUrl);
?>assets/lib/tablesorter/2.31.3/js/jquery.tablesorter.pager.min.js"></script>
<script src="<?php echo esc_url($templateUrl); ?>assets/js/script.js"></script>

</body>

</html>
