$(
    function () {

        $("table").tablesorter(
            {
                theme : "bootstrap",

                widthFixed: true,

                widgets : [ "filter", "columns", "zebra" ],

                widgetOptions : {
                    zebra : ["even", "odd"],
                    columns: [ "primary", "secondary", "tertiary" ],
                    filter_reset : ".reset",
                    filter_cssFilter: [
                        'form-control',
                        'form-control',
                        'form-control',
                    ]

                }
            }
        )
            .tablesorterPager(
                {

                    container: $(".ts-pager"),

                    cssGoto  : ".pagenum",

                    // remove rows from the table to speed up the sort of large tables.
                    // setting this to false, only hides the non-visible rows; needed if you plan to add/remove rows with the pager enabled.
                    removeRows: false,

                    // output string - default is '{page}/{totalPages}';
                    // possible variables: {page}, {totalPages}, {filteredPages}, {startRow}, {endRow}, {filteredRows} and {totalRows}
                    output: '{startRow} - {endRow} / {filteredRows} ({totalRows})'

                }
            );

    }
);
$(function () {

    let ajaxSetting = $('#ajax-settings');
    let userTable =  $('#user-table');
    let ajaxUrl = ajaxSetting.data('ajax-url');
    let userDetails = $('#dummy-user-details');
    let data = {};
    data.user_id = $(this).data('dummy-id');
    data.action = ajaxSetting.data('ajax-action');
    data._ajax_nonce = ajaxSetting.data('ajax-nonce');
    displayLoader();
    data.status = 'many';
        $.ajax({
            url: ajaxUrl,
            data: data,
            success: function (response) {
                hideLoader();
                if (response.success) {
                    let row = '';
                    $.each(response.data, function (index, user) {
                        row += createRow(user);
                    });

                    let _row = $(row);
                    let _resort = true;

                    userTable
                        .find('tbody').append(_row)
                        .trigger('addRows', [_row, _resort]);
                    userTable.fadeIn();
                } else {
                    let alert = createAlert(response.data, true);
                    alert.prependTo(userDetails);
                }
            },
            error: function (jqXHR) {
                hideLoader();
                let alert = createAlert(jqXHR.responseText);
                alert.prependTo(userDetails);

            }
        });

    return false;

});

let clicked = false;
$(document).on("click", "a.user-details", function (e) {
    e.preventDefault();

    if (!clicked) {
        clicked = true;
        let ajaxSetting = $('#ajax-settings');
        let ajaxUrl = ajaxSetting.data('ajax-url');
        let userDetails = $('#dummy-user-details');

        userDetails.hide(100);
        userDetails.toggle(500);

        let data = {};
        data.user_id = $(this).data('dummy-id');
        data.action = ajaxSetting.data('ajax-action');
        data._ajax_nonce = ajaxSetting.data('ajax-nonce');
        data.status = 'one';
        $(function () {
            userDetails.empty();
            displayLoader();
            $.ajax({
                url: ajaxUrl,
                data: data,
                success: function (response) {
                    hideLoader();
                    if (response.success) {
                        let details = createUserDetails(response);
                        userDetails.append(details);
                    } else {
                        let alert = createAlert(response.data);
                        alert.prependTo(userDetails);
                    }
                    clicked = false;
                },
                error: function (jqXHR) {
                    hideLoader();
                    let alert = createAlert(jqXHR.responseText);
                    alert.prependTo(userDetails);
                    clicked = false;
                }

            });
        })
    }

        return false;
});

$(document).on("click", "button.reload", function () {
    setTimeout(function () {
        location.reload();
    }, 600);
});

let createRow = user => {
    let trEl =  document.createElement('tr');

    let tdEl1 =  document.createElement('td');
    let tdEl2 =  document.createElement('td');
    let tdEl3 =  document.createElement('td');

    let aEl1 =  document.createElement('a');
    let aEl2 =  document.createElement('a');
    let aEl3 =  document.createElement('a');

    let a1 = $(aEl1).attr({'data-dummy-id':user.id, 'href':'#'})
        .addClass('user-details')
        .html(user.id);
    let a2 = $(aEl2).attr({'data-dummy-id':user.id, 'href':'#'})
        .addClass('user-details')
        .html(user.name);
    let a3 = $(aEl3).attr({'data-dummy-id':user.id, 'href':'#'})
        .addClass('user-details')
        .html(user.username);

    let td1 = $(tdEl1).html(a1);
    let td2 = $(tdEl2).html(a2);
    let td3 = $(tdEl3).html(a3);

    let tr = $(trEl).append(td1, td2, td3);
    return tr[0].outerHTML;

}
let createUserDetails = response => {

    let h5El = document.createElement('h5');
    let pEl = document.createElement('p');

    let user = response.data || {};
    let pContent = 'Email: '+  '<a href="email:' + user.email + '">' + user.email +'</a>. ';
    pContent+= 'Address: ' + user.addressFormatted+'. '   ;
    pContent+= 'Geo location: ' +user.coordinates + '. ';
    pContent+='Phone: <a href="tel:' + user.phoneFormatted + '">' + user.phone + '</a>. ';
    pContent+= 'Website: <a href="' + user.websiteFormatted + '" target="_blank">' + user.website + '</a>. ';
    pContent+= 'Company: ' + user.companyName + ', ';
    pContent+= 'Catch phrase: ' + user.companyCatchPhrase + ', ';
    pContent+= 'BS: ' + user.companyBs.replace(/\s/g, ', ') + '.';
    let h5Content = user.id + '. ' + user.name + ' ('+ user.username + '): ';
    let h5 =$(h5El).addClass('center')
        .html(h5Content);
    let p = $(pEl).addClass('small')
        .html(pContent);
    return h5.add(p);
}
let createAlert = (message, reload) => {

    let divEl = document.createElement('div');
    let spanEl = document.createElement('span');
    let buttonEl = document.createElement('button');
    let spanContent = '&times';
    let classReload ='';
    if (reload) {
        classReload = ' reload';
    }

    let span = $(spanEl).attr('aria-hidden', true)
        .html(spanContent);
    let button = $(buttonEl).attr({'type':'button', 'data-dismiss':'alert', 'aria-label':'Close'})
        .addClass('close' + classReload)
        .html(span);
     return $(divEl).attr({'role':'alert'})
        .addClass('alert alert-danger alert-dismissible fade show center')
        .prepend(message)
        .append(button);

}

let displayLoader = () => {
    $('#loadingDiv').hide()
    .show();
};

let hideLoader = () => {
    $('#loadingDiv').hide();
};