<?php
/**
* The following snippets uses `PLUGIN` to prefix
* the constants and class names. You should replace
* it with something that matches your plugin name.
 */
// define test environment
define('PLUGIN_PHPUNIT', true);

// define fake ABSPATH
if (!defined('ABSPATH')) {
    define('ABSPATH', sys_get_temp_dir());
}
// define fake DISPLAY_DUMMY_USERS_PATH
if (!defined('DISPLAY_DUMMY_USERS_PATH')) {
    define('DISPLAY_DUMMY_USERS_PATH', sys_get_temp_dir() . '/wp-content/plugins/display-dummy-users');
}

if (!defined('DDU_CACHE_DIR')) {
    define('DDU_CACHE_DIR', 'ddu_cache_dir_never_exists');
}

require_once __DIR__ . '/../../vendor/autoload.php';
