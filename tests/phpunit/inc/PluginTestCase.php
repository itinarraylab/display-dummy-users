<?php
namespace DisplayDummyUsersTests\Includes;

use PHPUnit\Framework\TestCase;
use Brain\Monkey;

/**
 * Class PluginTestCase
 * @package DisplayDummyUsers\Tests\Includes
 */
class PluginTestCase extends TestCase
{

    /**
     * Runs in the beginning of each test
     *
     * @return void;
     */
    protected function setUp(): void
    {
        Monkey\setUp();
        parent::setUp();
    }

    /**
     * Runs in the end of each test
     *
     * @return void;
     */
    protected function tearDown(): void
    {
        Monkey\tearDown();
        parent::tearDown();
    }

    /**
     * Allows to test private methods
     *
     * @param $object     'class'
     * @param $methodName 'method name'
     * @param array $parameters 'method params'
     *
     * @return mixed
     * @throws \ReflectionException
     */
    protected function invokeMethod(&$object, $methodName, array $parameters = [])
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    /**
     * Easier to use than assertEquals
     *
     * @param mixed  $expected    'Expected Output'
     * @param string $description 'Short Description'
     *
     * @return void;
     */
    protected function expectOutput($expected, $description = '')
    {
        $output = \ob_get_contents();
        \ob_clean();

        $output = \preg_replace('|\R|', "\r\n", $output);
        $expected = \preg_replace('|\R|', "\r\n", $expected);

        $this->assertEquals($expected, $output, $description);
    }


}
