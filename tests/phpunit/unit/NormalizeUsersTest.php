<?php

namespace DisplayDummyUsersTests\UnitCases;

use DisplayDummyUsers\Normalizers\NormalizeUsers;
use DisplayDummyUsersTests\Data\Info;
use DisplayDummyUsersTests\Includes\PluginTestCase;
use DisplayDummyUsersTests\Data\Many;
use DisplayDummyUsersTests\Data\One;

/**
 * @testdox NormalizeUsers::class
 */
class NormalizeUsersTest extends PluginTestCase {

    private $instance;
    private $currentClass;


    protected function setUp(): void {
        parent::setUp();
        $this->instance = \Mockery::mock(NormalizeUsers::class)
            ->shouldAllowMockingProtectedMethods()
            ->makePartial();
        $this->currentClass = new NormalizeUsers();
    }

    /**
     * @testdox one() returns empty or error
     *
     * @dataProvider errorsProviderOne
     */
    public function testOneReturnsEmptyOrError( $data, $expected )
    {

        $actual = NormalizeUsers::one($data);
        $this->assertSame($expected, $actual);

    }

    /**
     * @testdox one() returns correct response
     *
     * @dataProvider dataProviderOne
     */
    public function testOneReturnsCorrectResponse($data, $expected)
    {
        $actual = NormalizeUsers::one($data);
        $this->assertSame($expected, $actual);

    }

    /**
     * @testdox one() returns correct response with some nulls
     *
     * @dataProvider dataSomeEmptyProviderOne
     */
    public function testOneReturnsCorrectResponseWithSomeNulls($data, $expected)
    {
        $actual = NormalizeUsers::one($data);
        $this->assertSame($expected, $actual);

    }





    /**
     * @testdox many() returns empty or error
     *
     * @dataProvider errorsProviderMany
     */
    public function testManyReturnsEmptyOrError($data, $expected)
    {

        $actual = NormalizeUsers::many($data);
        $this->assertSame($expected, $actual);

    }

    /**
     * @testdox many() returns correct response
     *
     * @dataProvider dataProviderMany
     */
    public function testManyReturnsCorrectResponse($data, $expected)
    {
        $actual = NormalizeUsers::many($data);
        $this->assertSame($expected, $actual);

    }

    /**
     * @testdox formatPhone() returns correct response
     *
     * @dataProvider dataProviderPhone
     */
    public function testFormatPhoneReturnsCorrectResponse($data, $expected)
    {
        $actual=$this->invokeMethod(
            $this->currentClass,
            'formatPhone',
            [$data]
        );
        $this->assertSame($expected, $actual);

    }

    /**
     * @testdox formatWebsite() returns correct response
     *
     * @dataProvider dataProviderWebsite
     */
    public function testFormatWebsiteReturnsCorrectResponse($data, $expected)
    {
        $actual=$this->invokeMethod(
            $this->currentClass,
            'formatWebsite',
            [$data]
        );
        $this->assertSame($expected, $actual);

    }

    /**
     * @testdox formatAddress() returns correct response
     *
     * @dataProvider dataProviderAddress
     */
    public function testFormatAddressReturnsCorrectResponse($data, $expected)
    {
        $actual=$this->invokeMethod(
            $this->currentClass,
            'formatAddress',
            [$data]
        );
        $this->assertSame($expected, $actual);

    }

    /**
     * @testdox formatCoordinates() returns correct response
     *
     * @dataProvider dataProviderCoordinates
     */
    public function testFormatCoordinatesReturnsCorrectResponse($data, $expected)
    {
        $actual=$this->invokeMethod(
            $this->currentClass,
            'formatCoordinates',
            [$data]
        );
        $this->assertSame($expected, $actual);

    }

    /**
     * @testdox companyAttribute() returns correct response
     *
     * @dataProvider dataProviderCompany
     */
    public function testFormatCompanyReturnsCorrectResponse($data, $name,  $expected)
    {
        $actual=$this->invokeMethod(
            $this->currentClass,
            'companyAttribute',
            [$data,  $name]
        );
        $this->assertSame($expected, $actual);

    }


    /**
     * Provides error array to test one()
     *
     * @return array
     */
    public function errorsProviderOne()
    {
        return One::errorProvider();
    }

    /**
     * Provides data array to test one()
     *
     * @return array
     */
    public function dataProviderOne()
    {
        return One::dataProvider();
    }

    /**
     * Provides some empty data array to test one()
     *
     * @return array
     */
    public function dataSomeEmptyProviderOne()
    {
        return One::dataSomeEmptyProvider();
    }

    /**
     * Provides error array to test many()
     *
     * @return array
     */
    public function errorsProviderMany()
    {
        return Many::errorProvider();
    }

    /**
     * Provides data array to test many()
     *
     * @return array
     */
    public function dataProviderMany()
    {
        return Many::dataProvider();
    }

    /**
     * Provides data array to test formatPhone()
     *
     * @return array
     */
    public function dataProviderPhone()
    {
        return Info::phonesData();
    }

    /**
     * Provides data array to test formatWebsite()
     *
     * @return array
     */
    public function dataProviderWebsite()
    {
        return Info::websitesData();
    }

    /**
     * Provides data array to test formatAddress()
     *
     * @return array
     */
    public function dataProviderAddress()
    {
        return Info::addressesData();
    }

    /**
     * Provides data array to test formatCoordinates()
     *
     * @return array
     */
    public function dataProviderCoordinates()
    {
        return Info::coordinatesData();
    }

    /**
     * Provides data array to test companyAttribute()
     *
     * @return array
     */
    public function dataProviderCompany()
    {
        return Info::companiesData();
    }

}
