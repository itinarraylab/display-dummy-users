<?php

namespace DisplayDummyUsersTests\UnitCases;

use DisplayDummyUsers\Services\Loader;
use DisplayDummyUsersTests\Includes\PluginTestCase;

/**
 * @testdox Loader::class
 */
class LoaderTest extends PluginTestCase
{

    private $instance;
    private $currentClass;

    protected function setUp(): void
    {
        parent::setUp();
        $this->instance = \Mockery::mock(Loader::class)
                                  ->shouldAllowMockingProtectedMethods()
                                  ->makePartial();
        $this->currentClass = new Loader();
    }

    /**
     * @testdox getPublicPath() returns public folder path
     */
    public function testGetPublicPath()
    {
        $actual = $this->instance->getPublicPath();
        $this->assertStringStartsWith(DISPLAY_DUMMY_USERS_PATH, $actual);
        $this->assertStringEndsWith($this->instance::PUBLIC_PATH.DIRECTORY_SEPARATOR, $actual);
    }

    /**
     * @testdox render() returns false if no file to render
     */
    public function testRenderReturnsFalseNoFile()
    {
        $actual = $this->currentClass->render('fake-view');
        $this->assertFalse($actual);
    }

}
