<?php

namespace DisplayDummyUsersTests\UnitCases;

use DisplayDummyUsers\Services\Normalize;
use DisplayDummyUsersTests\Data\Info;
use DisplayDummyUsersTests\Includes\PluginTestCase;
use DisplayDummyUsersTests\Data\Many;
use DisplayDummyUsersTests\Data\One;

/**
 * @testdox Normalize::class
 */
class NormalizeTest extends PluginTestCase {


    /**
     * @testdox one() returns same
     *
     */
    public function testOneReturnsSame()
    {
        $data = ['foo', 'bar'];
        $actual = Normalize::one($data);
        $this->assertSame($data , $actual);

    }

    /**
     * @testdox many() returns same
     *
     */
    public function testManyReturnsSame()
    {
        $data = ['foo', 'bar'];
        $actual = Normalize::one($data);
        $this->assertSame($data , $actual);

    }

    /**
     * @testdox hasError() returns true on empty
     *
     */
    public function testHasErrorReturnsTrueOnEmpty()
    {
        $data = [];
        $actual = Normalize::hasError($data);
        $this->assertTrue($actual);

    }

    /**
     * @testdox hasError() returns true on error
     *
     */
    public function testHasErrorReturnsTrueOnError()
    {
        $data = ['error' => 'Some error'];
        $actual = Normalize::hasError($data);
        $this->assertTrue($actual);

    }

    /**
     * @testdox hasError() returns false for users data
     *
     */
    public function testHasErrorReturnsFalseForMany()
    {
        $data = Many::dataProvider();
        $actual = Normalize::hasError($data);
        $this->assertFalse($actual);

    }

    /**
     * @testdox hasError() returns false for user data
     *
     */
    public function testHasErrorReturnsFalseForOne()
    {
        $data = One::dataProvider();
        $actual = Normalize::hasError($data);
        $this->assertFalse($actual);

    }



}
