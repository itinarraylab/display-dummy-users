<?php

namespace DisplayDummyUsersTests\UnitCases;

use Brain\Monkey\Functions;
use DisplayDummyUsers\Services\Ajax;
use DisplayDummyUsersTests\Includes\PluginTestCase;

/**
 * @testdox Ajax::class
 */
class AjaxTest extends PluginTestCase
{

    /**
     * @var \Mockery\Mock
     */
    private $instance;

    /**
     * @var
     */
    private $currentClass;

    protected function setUp(): void
    {
        parent::setUp();
        $this->currentClass = new Ajax();
        $this->instance = \Mockery::mock($this->currentClass)
            ->shouldAllowMockingProtectedMethods()
            ->makePartial();
         Functions\when('__')
            ->returnArg(1);
    }

    /**
     * @testdox sendResponse() echoes success=>'false' and message on error
     *
     */
    public function testResponseOnError()
    {
        $response = ['error'=>'some error'];
        $extraMessage = ' and it is not working';
        $errorResponse = json_encode(
            ['success' => false, 'data'=>$response['error'].$extraMessage]
        );

        Functions\expect('wp_send_json_error')
            ->once()
            ->with($response['error'].$extraMessage)
            ->andReturnUsing(
                function () use ($errorResponse) {
                    echo $errorResponse;
                }
            );
        $this->invokeMethod(
            $this->currentClass,
            'sendResponse',
            [$response, $extraMessage]
        );
        $this->expectOutput($errorResponse);
    }

    /**
     * @testdox sendResponse() echoes success=>'false' and message on empty
     *
     */
    public function testResponseOnEmpty()
    {
        $response = [];
        $responsePartial =  sprintf(
            '<strong>%s</strong> Nothing was found. Try to modify request!',
            __('Oh, no...', 'ddu')
        );

        $errorResponse = json_encode(
            ['success' => false, 'data'=>$responsePartial]
        );

        Functions\expect('wp_send_json_error')
            ->once()
            ->with(
                $responsePartial
            )
            ->andReturnUsing(
                function () use ($errorResponse) {
                    echo $errorResponse;
                }
            );

        $this->invokeMethod(
            $this->currentClass,
            'sendResponse',
            [$response]
        );
        $this->expectOutput($errorResponse);
    }

    /**
     * @testdox sendResponse() echoes success=>'true' and message
     *
     */
    public function testResponseOnSuccess()
    {
        $response = ['id'=>1, 'name'=>'Leanne Graham'];

        $successResponse = json_encode(
            ['success' => true, 'data'=>$response]
        );

        Functions\expect('wp_send_json_success')
            ->once()
            ->with($response)
            ->andReturnUsing(
                function () use ($successResponse) {
                    echo $successResponse;
                }
            );
        $this->invokeMethod(
            $this->currentClass,
            'sendResponse',
            [$response]
        );
        $this->expectOutput($successResponse);
    }

    /**
     * @testdox ajaxData() returns data array
     *
     */
    public function testAjaxData()
    {
        $expected = [
                'ajaxAction' => $this->currentClass::ACTION,
                'ajaxNonce' => $this->currentClass::NONCE,
                'ajaxUrl' => $this->currentClass::ADMIN_URL,
            ];

        Functions\expect('wp_create_nonce')
            ->once()
            ->with($this->currentClass::NONCE)
            ->andReturn($this->currentClass::NONCE);

        Functions\expect('admin_url')
            ->once()
            ->with($this->currentClass::ADMIN_URL)
            ->andReturn($this->currentClass::ADMIN_URL);

         $actual = $this->instance->ajaxData();
         $this->assertSame($expected, $actual);
    }

    /**
     * @testdox handle() empty or wrong status error check
     *
     */
    public function testHandleNoOrWrongStatus()
    {
        Functions\expect('check_ajax_referer')
            ->once()
            ->with($this->instance::NONCE)
            ->andReturnUsing(
                function () {
                    return 1;
                }
            );
        $responsePartial =  sprintf(
            '<strong>%s</strong>Your request was not recognized. Try a different one!',
            __('Oh, no...', 'ddu')
        );

        $errorResponse = json_encode(
            ['success' => false, 'data'=>$responsePartial]
        );

        Functions\expect('wp_send_json_error')
            ->with(
                $responsePartial
            )
            ->andReturnUsing(
                function () use ($errorResponse) {
                    echo $errorResponse;
                    return;
                }
            );

        $this->instance->handle();
        $this->expectOutput($errorResponse);
    }

    /**
     * @testdox register() fires hooks wp_ajax and wp_ajax_nopriv
     *
     */
    public function testRegisterFiresHooks()
    {
        Functions\expect('check_ajax_referer')
            ->with(
                'string'
            )
            ->andReturnUsing(
                function () {
                    return false;
                }
            );
        Functions\expect('wp_send_json_error')
            ->with(
                'Foo'
            )
            ->andReturnUsing(
                function () {
                    return 'Foo';
                }
            );

        $this->currentClass->register();
        self::assertTrue(
            has_action(
                'wp_ajax_' . $this->currentClass::ACTION,
                $this->currentClass->handle()
            )
        );
        self::assertTrue(
            has_action(
                'wp_ajax_nopriv_' . $this->currentClass::ACTION,
                $this->currentClass->handle()
            )
        );
    }
}
