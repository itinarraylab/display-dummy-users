<?php

namespace DisplayDummyUsersTests\UnitCases;

use DisplayDummyUsers\Services\Cache;
use DisplayDummyUsersTests\Data\Info;
use DisplayDummyUsersTests\Includes\PluginTestCase;

/**
 * @testdox Cache::class
 */
class CacheTest extends PluginTestCase
{

    private $instance;
    private $currentClass;

    protected function setUp(): void
    {
        parent::setUp();
        $this->instance = \Mockery::mock(Cache::class)
                                  ->shouldAllowMockingProtectedMethods()
                                  ->makePartial();
        $this->currentClass = new Cache();
    }

    /**
     * @testdox write() returns null, no errors on not writable file
     */
    public function testWriteReturnsVoid()
    {
        $label = 'file_name';
        $data = 'string';
        $actual = $this->currentClass->write( $label, $data);
        $this->assertNull($actual);
    }

    /**
     * @testdox read() returns empty string, no errors disabled cache
     */
    public function testReadReturnsEmptyOnDisabledCached()
    {
        $label = 'file_name';
        $actual = $this->currentClass->read($label);
        $this->assertEmpty($actual);
    }

    /**
     * @testdox read() returns empty string, no errors on file doesn't exist
     */
    public function testReadReturnsEmptyOnNoFile()
    {
        $label = 'file_name';
        $this->instance->expects('isCached')
            ->andReturn(true);
        $actual = $this->instance->read($label);
        $this->assertEmpty($actual);
    }

    /**
     * @testdox flush() returns false, no errors
     */
    public function testFlushReturnsFalse()
    {
        $actual = $this->currentClass->flush();
        $this->assertFalse($actual);
    }

    /**
     * @testdox isCached() returns false, no errors
     *
     */
    public function testIsCachedReturnsFalse()
    {
        $label = 'file_name';
        $actual = $this->currentClass->isCached($label);
        $this->assertFalse($actual);

    }

    /**
     * @testdox safeFileName() returns false, no errors
     * @dataProvider dataProviderFilenames
     */
    public function testSafeFileName($data, $expected)
    {
        $actual=$this->invokeMethod(
            $this->currentClass,
            'safeFileName',
            [$data]
        );
        $this->assertSame($expected, $actual);
    }

    /**
     * @testdox isCacheEnabled() returns false, no errors
     *
     */
    public function testIsCacheEnabledReturnsFalse()
    {

        $actual = $this->currentClass->isCacheEnabled();
        $this->assertFalse($actual);

    }

    /**
     * @testdox cacheDir() returns empty string, no errors
     *
     */
    public function testCacheDirReturnsEmptyString()
    {

        $actual = $this->currentClass->cacheDir();
        $this->assertEmpty($actual);

    }

    /**
     * @testdox cacheDir() returns cacheDir, no errors
     *
     */
    public function testCacheDirReturnsDir()
    {
        $this->instance->expects('isCacheEnabled')
            ->andReturn(true);
        $actual = $this->instance->cacheDir();
        $this->assertSame(DDU_CACHE_DIR, $actual);

    }



    /**
     * Provides data array to test companyAttribute()
     *
     * @return array
     */
    public function dataProviderFilenames()
    {
        return Info::fileNamesData();
    }

}
