<?php

namespace DisplayDummyUsersTests\UnitCases;

use Brain\Monkey\Functions;
use DisplayDummyUsers\Load;
use DisplayDummyUsersTests\Includes\PluginTestCase;


/**
 * @testdox Load::class
 */
class LoadTest extends PluginTestCase
{

    private $instance;
    private $currentClass;

    protected function setUp(): void
    {
        parent::setUp();
        $this->instance = \Mockery::mock(Load::class)
                                  ->shouldAllowMockingProtectedMethods()
                                  ->makePartial();
        $this->currentClass = new Load();
    }

    /**
     * @testdox addQueryVars() adding my_url const to $vars
     */
    public function testAddQueryVars()
    {
        $vars = ['foo', 'bar'];
        $valAdded = $this->currentClass::MY_URL;
        $actual =$this->currentClass->addQueryVars($vars);
        array_push($vars, $valAdded);
        $this->assertSame($vars, $actual);
    }
    /**
     * @testdox init() fires hooks query_vars and template_redirect
     *
     */
    public function testInitFiresHooks()
    {
        Functions\expect('check_ajax_referer')
            ->with(
                'string'
            )
            ->andReturnUsing(
                function () {
                    return false;
                }
            );

        Functions\expect('wp_create_nonce')
            ->with(
                'Foo'
            )
            ->andReturnUsing(
                function () {
                    return 'Foo';
                }
            );
        Functions\expect('admin_url')
            ->with(
                'Foo'
            )
            ->andReturnUsing(
                function () {
                    return 'Foo';
                }
            );
        $this->currentClass::init();
        self::assertTrue(
            has_filter(
                'query_vars',
                $this->currentClass::init()
            )
        );
        self::assertTrue(
            has_action(
                'template_redirect',
                $this->currentClass::init()
            )
        );
    }

    /**
     * @testdox wpQuery() returns \WP_Query object
     */
    public function testWpQuery()
    {
        global $wp_query;
        $wp_query = \Mockery::mock('overload:\WP_Query');
        $actual = $this->currentClass->wpQuery();
        $this->assertSame($wp_query, $actual);
    }

    /**
     * @testdox userTemplate() returns template if not plugin url
     */
    public function testUserTemplate()
    {
        $template = 'template';
        $vars = [$this->currentClass::MY_URL=>'users'];
        $wp_query = \Mockery::mock('overload:\WP_Query');
        $wp_query->shouldReceive('set')
                 ->andReturn($vars)
            ->andSet('query', $vars);
         $wp_query->set($vars);
        $this->instance->expects('wpQuery')
            ->andReturn($wp_query);
        $this->instance->expects('output')
            ->andReturn(false);

        $actual = $this->instance->userTemplate($template);
        $this->assertSame($template, $actual);
    }
}
