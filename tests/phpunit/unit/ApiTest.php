<?php

namespace DisplayDummyUsersTests\UnitCases;

use DisplayDummyUsers\Services\Api;
use DisplayDummyUsersTests\Data\Many;
use DisplayDummyUsersTests\Includes\PluginTestCase;
use Brain\Monkey\Functions;

/**
 * @testdox Api::class
 */
class ApiTest extends PluginTestCase
{

    private $currentClass;

    protected function setUp(): void
    {
        parent::setUp();
        $this->currentClass = new Api();
    }

    /**
     * @testdox get() returns response
     */
    public function testGetResponse()
    {
        $response = Many::dataResponse();

        Functions\when('wp_remote_get')
            ->justReturn($response);
        Functions\when('is_wp_error')
            ->justReturn(false);

        $actual = $this->invokeMethod(
            $this->currentClass,
            'get',
            [$this->currentClass::BASE_URL]
        );
        $this->assertEquals($response, $actual);
    }

    /**
     * @testdox get() returns error
     */
    public function testGetError()
    {
        $errorMessage = 'Too many requests';
        $expected = ['error' => $errorMessage];

        $wpError = \Mockery::mock('overload:\WP_Error');
        $wpError->shouldReceive('get_error_message')
                 ->andReturn($errorMessage);

        Functions\when('wp_remote_get')
            ->justReturn($wpError);
        Functions\when('is_wp_error')
            ->justReturn($wpError);

        $actual = $this->invokeMethod(
            $this->currentClass,
            'get',
            [$this->currentClass::BASE_URL]
        );
        $this->assertEquals($expected , $actual);
    }

    /**
     * @testdox buildUrl() without resource identifier
     */
    public function testBuildUrlNoIdentifier()
    {
        $resource = 'users';
        $expected = $this->currentClass::BASE_URL.$resource;
        $actual = $this->currentClass->buildUrl($resource);
        $this->assertEquals($expected , $actual);

    }

    /**
     * @testdox buildUrl() with resource identifier
     */
    public function testBuildUrlWithIdentifier()
    {
        $resource = 'users';
        $resourceIdentifier = '1';
        $expected = $this->currentClass::BASE_URL.$resource.$this->currentClass::PATH_SEP.$resourceIdentifier;
        $actual = $this->currentClass->buildUrl($resource, $resourceIdentifier);
        $this->assertEquals($expected , $actual);

    }

}
