<?php

namespace DisplayDummyUsersTests\UnitCases;

use DisplayDummyUsers\Deactivate;
use DisplayDummyUsersTests\Includes\PluginTestCase;

/**
 * @testdox Deactivate::class
 */
class DeactivateTest extends PluginTestCase
{


    /**
     * @testdox init() returns void on deactivation
     */
    public function testGetPublicPath()
    {
        $actual = Deactivate::init();
        $this->assertNull($actual);

    }

}
