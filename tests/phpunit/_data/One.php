<?php
namespace DisplayDummyUsersTests\Data;

class One {

    /**
     * Returns data for tests
     *
     * @return array
     */
    public static function dataProvider(): array
    {

        return [

                [
                    [
                        'id'       => '1',
                        'name'     => 'Name1',
                        'username' => 'Username1',
                        'email'    => 'email@test.com',
                        'address'  =>
                            [
                                'street'  => 'Street 1',
                                'suite'   => 'Suite 1',
                                'city'    => 'City 1',
                                'zipcode' => '11111',
                                'geo'     => [
                                    'lat' => '111,11',
                                    'lng' => '-111,11',
                                ]
                            ],
                        'phone' =>'(111) 111-1111',
                        'website' =>'some1.com',
                        'company' =>
                            [
                                'name' => 'Name1',
                                'catchPhrase' => 'Catch Phrase1',
                                'bs' => 'one two three1',
                            ]

                    ],


                    [

                        'id'                 => '1',
                        'name'               => 'Name1',
                        'username'           => 'Username1',
                        'email'              => 'email@test.com',
                        'phone'              => '(111) 111-1111',
                        'phoneFormatted'     => '111-111-1111',
                        'addressFormatted'   => 'Street 1 Suite 1 City 1 11111',
                        'coordinates'        => '111,11, -111,11',
                        'website'            => 'some1.com',
                        'companyName'        => 'Name1',
                        'companyCatchPhrase' => 'Catch Phrase1',
                        'companyBs'          => 'one two three1',
                        'websiteFormatted'   => '//some1.com',

                    ]

                ]

        ];
    }

    /**
     * Returns data for tests
     *
     * @return array
     */
    public static function dataSomeEmptyProvider(): array
    {

        return [

            [
                [
                    'id'       => '1',
                    'name'     => 'Name1',
                    'username' => 'Username1',
                    'address'  =>
                        [
                            'street'  => 'Street 1',
                            'city'    => 'City 1',
                            'zipcode' => '11111',
                            'geo'     => [
                                'lat' => '111,11',
                                'lng' => '-111,11',
                            ]
                        ],
                    'phone' =>'(111) 111-1111 x12322',
                    'website' =>'https://some1.com',
                    'company' =>
                        [
                            'name' => 'Name1',
                            'catchPhrase' => 'Catch Phrase1',
                            'bs' => 'one two three1',
                        ]

                ],


                [

                    'id'                 => '1',
                    'name'               => 'Name1',
                    'username'           => 'Username1',
                    'email'              => '',
                    'phone'              => '(111) 111-1111 x12322',
                    'phoneFormatted'     => '111-111-1111p12322',
                    'addressFormatted'   => 'Street 1 City 1 11111',
                    'coordinates'        => '111,11, -111,11',
                    'website'            => 'https://some1.com',
                    'companyName'        => 'Name1',
                    'companyCatchPhrase' => 'Catch Phrase1',
                    'companyBs'          => 'one two three1',
                    'websiteFormatted'   => '//some1.com',

                ]

            ]

        ];
    }

    /**
     * Returns error set for tests
     *
     * @return array
     */
    public static function errorProvider(): array
    {

        return [
            [
                [],
                []
            ],
            [
                [
                    'error' => 'Error'
                ],
                [
                    'error' => 'Error'
                ]
            ],

        ];
    }

}