<?php
namespace DisplayDummyUsersTests\Data;

class Info {

    /**
     * Returns data for test phones
     *
     * @return array
     */
    public static function phonesData()
    {

        return [
            ['881-222-0002', '881-222-0002'],
            ['881-222-0002 x2345', '881-222-0002p2345'],
            ['(881) 222-0002 x2345', '881-222-0002p2345'],
            ['(881) 222 0002 x2345', '881-222-0002p2345'],
        ];


    }

    /**
     * Returns data for test websites
     *
     * @return array
     */
    public static function websitesData()
    {

        return [
            ['foo.bar', '//foo.bar'],
            ['http://foo.bar', '//foo.bar'],
            ['https://foo.bar', '//foo.bar'],
            ['https://www.foo.bar', '//www.foo.bar'],
            ['http://www.foo.bar', '//www.foo.bar'],
            ['www.foo.bar', '//www.foo.bar'],

        ];


    }

    /**
     * Returns data for test addresses
     *
     * @return array
     */
    public static function addressesData()
    {

        return [
            [[], ''],
            [
                [
                'street'  => 'Street 2',
                'suite'   => 'Suite 2',
                'city'    => 'City 2',
                'zipcode' => '11121',
                'geo'     =>
                    [
                    'lat' => '111,22',
                    'lng' => '-111,22',
                    ]

                ], 'Street 2 Suite 2 City 2 11121'
            ],
            [
                [
                    'street'  => 'Street 2',
                    'suite'   => 'Suite 2',
                    'city'    => 'City 2',
                    'zipcode' => '11121',

                ], 'Street 2 Suite 2 City 2 11121'
            ],
            [
                [
                    'street'  => 'Street 2',
                    'suite'   => 'Suite 2',
                    'city'    => 'City 2',


                ], 'Street 2 Suite 2 City 2'
            ],
            [
                [
                    'street'  => 'Street 2',
                    'city'    => 'City 2',
                    'zipcode' => '11121',

                ], 'Street 2 City 2 11121'
            ],




        ];


    }

    /**
     * Returns data for test coordinates
     *
     * @return array
     */
    public static function coordinatesData()
    {

        return [
            [[], ''],
            [
                'geo'     =>
                 [
                     'lat' => '111,22',
                     'lng' => '-111,22',
                 ],
                ''

            ],
            [
                'address'=>[
                    'street'  => 'Street 2',
                    'suite'   => 'Suite 2',
                    'city'    => 'City 2',
                    'zipcode' => '11121',
                    'geo'     =>
                        [
                            'lat' => '111,22',
                            'lng' => '-111,22',
                        ]

                ],
                '111,22, -111,22'

            ],
            [
                'address'=>[
                    'street'  => 'Street 2',
                    'suite'   => 'Suite 2',
                    'city'    => 'City 2',
                    'zipcode' => '11121',
                    'geo'     =>
                        [
                            'lat' => '111,22',

                        ]

                ],
                ''

            ],
            [
                'address'=>[
                    'street'  => 'Street 2',
                    'suite'   => 'Suite 2',
                    'city'    => 'City 2',
                    'zipcode' => '11121',
                    'geo'     =>
                        [
                            'lng' => '111,22',

                        ]

                ],
                ''

            ],
            [
                'address'=>[
                    'street'  => 'Street 2',
                    'suite'   => 'Suite 2',
                    'city'    => 'City 2',
                    'zipcode' => '11121',


                ],
                ''

            ],



        ];


    }

    /**
     * Returns data for test company attributes
     *
     * @return array
     */
    public static function companiesData()
    {

        return [
            [[], '', ''],
            [
                [
                    'name' => 'Name2',
                    'catchPhrase' => 'Catch Phrase2',
                    'bs' => 'one two, three2',
                ]
                , '', ''
            ],
            [
                [
                    'name' => 'Name2',
                    'catchPhrase' => 'Catch Phrase2',
                    'bs' => 'one two, three2',
                ]
                , 'name', 'Name2'
            ],
            [
                [
                    'name' => 'Name2',
                    'catchPhrase' => 'Catch Phrase2',
                    'bs' => 'one two, three2',
                ]
                , 'catchPhrase', 'Catch Phrase2'
            ],
            [
                [
                    'name' => 'Name2',
                    'catchPhrase' => 'Catch Phrase2',
                    'bs' => 'one two, three2',
                ]
                , 'bs', 'one two, three2'
            ],


        ];

    }

    /**
     * Returns data for test Cache::safeFilename
     *
     * @return array
     */
    public static function fileNamesData()
    {
        return [
          ['', ''],
          ['prefix_*&^@', 'prefix_'],
          ['prefix_039c3124738f988c59f77095e1da9dcd', 'prefix_039c3124738f988c59f77095e1da9dcd'],
          ['prefix_039c3124738f988c59f77095e1da9dcd!', 'prefix_039c3124738f988c59f77095e1da9dcd'],
          ['prefix_039c3124738f988c59f77095e1da9dcd! ', 'prefix_039c3124738f988c59f77095e1da9dcd'],
        ];
    }

}
