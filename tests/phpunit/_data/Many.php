<?php
namespace DisplayDummyUsersTests\Data;

class Many {

    /**
     * Returns data for tests
     *
     * @return array
     */
    public static function dataProvider(): array
    {

        return [

            [
                [
                    [
                        'id'       => '1',
                        'name'     => 'Name1',
                        'username' => 'Username1',
                        'email'    => 'email@test.com',
                        'address'  =>
                            [
                                'street'  => 'Street 1',
                                'suite'   => 'Suite 1',
                                'city'    => 'City 1',
                                'zipcode' => '11111',
                                'geo'     => [
                                    'lat' => '111,11',
                                    'lng' => '-111,11',
                                ]
                            ],
                        'phone' =>'(111) 111-1111',
                        'website' =>'some1.com',
                        'company' =>
                            [
                                'name' => 'Name1',
                                'catchPhrase' => 'Catch Phrase1',
                                'bs' => 'one two, three1',
                            ]

                    ]
                ],
                [
                    [

                        'id'       => '1',
                        'name'     => 'Name1',
                        'username' => 'Username1',
                    ]


                ]
            ],

            [
                [
                    [
                        'id'       => '2',
                        'name'     => 'Name2',
                        'username' => 'Username2',
                        'email'    => 'email2@test.com',
                        'address'  =>
                            [
                                'street'  => 'Street 2',
                                'suite'   => 'Suite 2',
                                'city'    => 'City 2',
                                'zipcode' => '11121',
                                'geo'     => [
                                    'lat' => '111,22',
                                    'lng' => '-111,22',
                                ]
                            ],
                        'phone' =>'(111) 111-1112',
                        'website' =>'some2.com',
                        'company' =>
                            [
                                'name' => 'Name2',
                                'catchPhrase' => 'Catch Phrase2',
                                'bs' => 'one two, three2',
                            ]

                    ]
                ],
                [
                    [

                        'id'       => '2',
                        'name'     => 'Name2',
                        'username' => 'Username2',
                    ]


                ]
            ],


        ];
    }

    /**
     * Returns error set for tests
     *
     * @return array
     */
    public static function errorProvider(): array
    {

        return [
            [ [], [] ],
            [
                [
                    'error' => 'Error'
                ],
                [
                    'error' => 'Error'
                ]
            ],
            [
                [
                    'name'     => 'Name',
                    'username' => 'Username',
                    'email'    => 'email',
                ],
                []
            ],

        ];
    }

    /**
     * Returns data for tests
     *
     * @return array
     */
    public static function dataResponse(): array
    {

        return [
                [
                    'id'       => '1',
                    'name'     => 'Name1',
                    'username' => 'Username1',
                    'email'    => 'email@test.com',
                    'address'  =>
                        [
                            'street'  => 'Street 1',
                            'suite'   => 'Suite 1',
                            'city'    => 'City 1',
                            'zipcode' => '11111',
                            'geo'     => [
                                'lat' => '111,11',
                                'lng' => '-111,11',
                            ]
                        ],
                    'phone' =>'(111) 111-1111',
                    'website' =>'some1.com',
                    'company' =>
                        [
                            'name' => 'Name1',
                            'catchPhrase' => 'Catch Phrase1',
                            'bs' => 'one two, three1',
                        ]

                ],

                [
                    'id'       => '2',
                    'name'     => 'Name2',
                    'username' => 'Username2',
                    'email'    => 'email2@test.com',
                    'address'  =>
                        [
                            'street'  => 'Street 2',
                            'suite'   => 'Suite 2',
                            'city'    => 'City 2',
                            'zipcode' => '11121',
                            'geo'     => [
                                'lat' => '111,22',
                                'lng' => '-111,22',
                            ]
                        ],
                    'phone' =>'(111) 111-1112',
                    'website' =>'some2.com',
                    'company' =>
                        [
                            'name' => 'Name2',
                            'catchPhrase' => 'Catch Phrase2',
                            'bs' => 'one two, three2',
                        ]

                ],


        ];
    }

}