<?php declare(strict_types=1);
/**
Plugin Name: Display Dummy Users
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: A brief description of the Plugin.
Version: 1.0
Author: Kostiantyn Aleksieiev <const.ca@gmail.com>
Author URI: http://URI_Of_The_Plugin_Author
License: MIT
PHP version: 7.3
 */

require_once 'ddu-config.php';
add_action('plugins_loaded', [DisplayDummyUsers\Load::class, 'init']);
register_deactivation_hook(__FILE__, [DisplayDummyUsers\Deactivate::class, 'init']);
