<?php declare(strict_types=1);
/**
 * Define Constants
 * PHP version 7.3
 */

if (!defined('DISPLAY_DUMMY_USERS_PATH')) {
    define('DISPLAY_DUMMY_USERS_PATH', plugin_dir_path(__FILE__));
}
if (!defined('DISPLAY_DUMMY_USERS_URL')) {
    define('DISPLAY_DUMMY_USERS_URL', plugin_dir_url(__FILE__));
}

if (!defined('DISPLAY_DUMMY_LOCAL_ABR')) {
    define('DISPLAY_DUMMY_LOCAL_ABR', 'ddu');
}
